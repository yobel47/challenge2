export const primaryColor = '#D3D9FD';
export const secondaryColor = '#091B6F';
export const blueColor = '#0D28A6';
export const greenColor = '#5CB85F';
export const lightGreenColor = '#DEF1DF';
export const grayColor = '#8A8A8A';
export const backgroundColor = '#F4F4F4';
export const largeFont = 12;
