import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {Splash} from '../pages';
import BottomNavigator from '../components/BottomNavigator';

const Stack = createStackNavigator();

const Router = () => {
  return (
    <Stack.Navigator initialRouteName="Splash">
      <Stack.Screen
        name="Splash"
        component={Splash}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="MainApp"
        component={BottomNavigator}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

export default Router;
