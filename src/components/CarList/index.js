import {StyleSheet, SafeAreaView, View, FlatList} from 'react-native';
import React from 'react';
import CarListItem from '../CarListItem';
import {AnotherMercedes} from '../../assets';

const CarList = ({header, footer}) => {
  const data = require('../../data/carListData.json');

  return (
    <FlatList
      style={{
        width: '100%',
        flex: 1,
        marginBottom: 24,
      }}
      data={data}
      renderItem={({item}) => (
        <CarListItem
          CarImage={AnotherMercedes}
          title={item.title}
          passenger={item.passenger}
          brief={item.brief}
          price={item.price}
        />
      )}
      ListHeaderComponent={header}
      ListFooterComponent={footer}
    />
  );
};

export default CarList;

const styles = StyleSheet.create({});
