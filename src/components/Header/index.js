import {
  StyleSheet,
  Text,
  View,
  Image,
  ImageBackground,
  Dimensions,
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import React from 'react';
import {Photo} from '../../assets';
import {primaryColor} from '../../utils/constant';

const Header = ({name, location}) => {
  return (
    <ImageBackground style={styles.background}>
      <View style={styles.header}>
        <View>
          <Text style={styles.name}>Hi, {name}!</Text>
          <Text style={styles.location}>
            <FontAwesome name="map-marker" style={styles.icon} />
            {location}
          </Text>
        </View>
        <View>
          <Image source={Photo} style={styles.photo} />
        </View>
      </View>
    </ImageBackground>
  );
};

export default Header;

const window = Dimensions.get('window');

const styles = StyleSheet.create({
  background: {
    width: '100%',
    height: window.height * 0.23,
    backgroundColor: primaryColor,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 24,
  },
  photo: {
    borderWidth: 2,
    borderColor: 'black',
    borderRadius: 50,
    width: window.height * 0.075,
    height: window.height * 0.075,
  },
  name: {
    fontWeight: '300',
    fontSize: 18,
    paddingBottom: 8,
    color: 'black',
    fontFamily: 'Helvetica',
  },
  icon: {
    fontWeight: '700',
    fontSize: 21,
    letterSpacing: 10,
  },
  location: {
    color: 'black',
    fontWeight: '700',
    fontSize: 21,
    fontFamily: 'Helvetica',
  },
});
