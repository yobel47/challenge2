import {StyleSheet, View} from 'react-native';
import React from 'react';
import MenuItem from '../MenuItem';
import {FlatList} from 'react-native-gesture-handler';

const Menu = () => {
  const data = [
    {title: 'Sewa Mobil'},
    {title: 'Oleh-Oleh'},
    {title: 'Penginapan'},
    {title: 'Wisata'},
  ];
  return (
    <FlatList
      contentContainerStyle={styles.menu}
      data={data}
      renderItem={({item}) => <MenuItem title={item.title} />}
      horizontal={true}
    />
  );
};

export default Menu;

const styles = StyleSheet.create({
  menu: {
    flexGrow: 1,
    paddingHorizontal: 24,
    paddingTop: 24,
    paddingBottom: 6,
    marginTop: 8,
    justifyContent: 'space-between',
  },
});
