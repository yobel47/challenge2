import {StyleSheet, Text, View, Dimensions} from 'react-native';
import React from 'react';
import Button from '../Button';
import {Mercedes} from '../../assets';
import {blueColor, secondaryColor} from '../../utils/constant';

const Banner = () => {
  return (
    <View style={styles.banner}>
      <View style={{flex: 1}}>
        <Text style={styles.bannerTitle}>
          Sewa Mobil Berkualitas di kawasanmu
        </Text>
        <Button title={'Sewa Mobil'} />
      </View>
      <View
        style={{
          flex: 1,
          justifyContent: 'flex-end',
          alignItems: 'flex-end',
          alignContent: 'flex-end',
          alignSelf: 'flex-end',
        }}>
        <View style={{flex: 2}}></View>
        <View style={styles.carBackground}>
          <Mercedes
            height={window.height * 0.6}
            width={window.width * 0.45}
            style={styles.car}
          />
        </View>
      </View>
    </View>
  );
};

export default Banner;

const window = Dimensions.get('window');

const styles = StyleSheet.create({
  banner: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    marginTop: -window.height * 0.095,
    borderRadius: 8,
    marginHorizontal: 24,
    backgroundColor: secondaryColor,
  },
  bannerTitle: {
    fontFamily: 'Helvetica',
    fontWeight: '400',
    color: 'white',
    fontSize: 23,
    paddingBottom: 16,
    paddingTop: 24,
    paddingLeft: 24,
    marginRight: -110,
  },
  car: {
    marginTop: -window.height * 0.29,
    marginBottom: -window.height * 1,
  },
  carBackground: {
    borderTopLeftRadius: 60,
    height: '100%',
    width: '100%',
    flex: 1,
    backgroundColor: blueColor,
    alignItems: 'flex-end',
    borderBottomRightRadius: 8,
  },
});
