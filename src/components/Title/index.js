import {StyleSheet, Text, View} from 'react-native';
import React from 'react';

const Title = ({title}) => {
  return (
    <View style={styles.containerTitle}>
      <Text style={styles.titlePage}>{title}</Text>
    </View>
  );
};

export default Title;

const styles = StyleSheet.create({
  containerTitle: {
    alignSelf: 'flex-start',
    alignItems: 'flex-start',
  },
  titlePage: {
    fontFamily: 'Helvetica',
    fontWeight: '700',
    fontSize: 20,
    color: 'black',

    textAlign: 'right',
    marginHorizontal: 24,
    marginVertical: 24,
  },
});
