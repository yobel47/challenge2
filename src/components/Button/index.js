import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {greenColor} from '../../utils/constant';

const Button = ({title}) => {
  return (
    <View>
      <TouchableOpacity style={styles.btn}>
        <Text style={styles.btnText}>{title}</Text>
      </TouchableOpacity>
    </View>
  );
};

export default Button;

const styles = StyleSheet.create({
  btn: {
    borderRadius: 2,
    elevation: 8,
    marginHorizontal: 24,
    marginBottom: 24,
    paddingVertical: 8,
    paddingHorizontal: 20,
    backgroundColor: greenColor,
  },
  btnText: {
    textAlign: 'center',
    fontFamily: 'Helvetica',
    fontWeight: 'bold',
    fontSize: 16,
    color: 'white',
  },
});
