import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import React from 'react';
import Feather from 'react-native-vector-icons/Feather';
import {greenColor, lightGreenColor} from '../../utils/constant';

const MenuItem = ({title}) => {
  const Icon = () => {
    if (title === 'Sewa Mobil')
      return <Feather name="truck" style={styles.icon} />;
    if (title === 'Oleh-Oleh')
      return <Feather name="box" style={styles.icon} />;
    if (title === 'Penginapan')
      return <Feather name="key" style={styles.icon} />;
    if (title === 'Wisata')
      return <Feather name="camera" style={styles.icon} />;

    return <Feather name="truck" style={styles.icon} />;
  };

  return (
    <View style={{alignItems: 'center', flex: 1}}>
      <TouchableOpacity>
        <View style={styles.menuIcon}>
          <Icon />
        </View>
      </TouchableOpacity>
      <Text style={styles.menuText}>{title}</Text>
    </View>
  );
};

export default MenuItem;

const styles = StyleSheet.create({
  icon: {
    margin: 16,
    color: greenColor,
    fontSize: 24,
  },
  menuIcon: {
    alignItems: 'center',
    backgroundColor: lightGreenColor,
    borderRadius: 8,
  },
  menuText: {
    fontSize: 15,
    paddingTop: 8,
    fontWeight: '300',
    fontFamily: 'Helvetica',
    color: 'black',
  },
});
