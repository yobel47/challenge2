import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import React from 'react';
import {grayColor, greenColor} from '../../utils/constant';

const CarListItem = ({CarImage, title, passenger, brief, price}) => {
  return (
    <View>
      <TouchableOpacity style={styles.container}>
        <View style={styles.photoBackground}>
          <CarImage height={window.height * 0.1} width={window.width * 0.17} />
        </View>
        <View style={styles.containerText}>
          <View>
            <Text style={styles.carTitle}>{title}</Text>
          </View>
          <View>
            <Text>
              <Feather name="users" style={styles.icon} />
              <Text style={styles.detailText}>{passenger}</Text>
              <View style={{padding: 10}}></View>
              <Feather name="briefcase" style={styles.icon} />
              <Text style={styles.detailText}>{brief}</Text>
            </Text>
          </View>
          <View>
            <Text style={styles.priceText}>Rp. {price}</Text>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default CarListItem;

const window = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginVertical: 8,
    marginHorizontal: 24,
    padding: 24,
    borderRadius: 4,
    overflow: 'hidden',
    shadowRadius: 8,
    borderRadius: 8,
    elevation: 3,
    backgroundColor: 'white',
  },
  photoBackground: {
    flex: 1,
  },
  photo: {
    width: 70,
    height: 44,
    marginTop: 5,
  },
  containerText: {
    marginHorizontal: 8,
    flex: 3,
  },
  carTitle: {
    fontFamily: 'Helvetica',
    fontWeight: '400',
    fontSize: 20,
    color: 'black',
  },
  detailText: {
    marginTop: 4,
    fontFamily: 'Helvetica',
    fontWeight: '300',
    fontSize: 16,
    color: grayColor,
  },
  icon: {
    margin: 16,
    color: grayColor,
    fontSize: 16,
    letterSpacing: 12,
  },
  priceText: {
    paddingTop: 8,
    fontSize: 16,
    color: greenColor,
    lineHeight: 20,
  },
});
