import {StyleSheet, SafeAreaView, View} from 'react-native';
import React from 'react';
import Title from '../../components/Title';
import {CarList} from '../../components';
import {backgroundColor} from '../../utils/constant';

const Mobil = () => {
  const header = (
    <View style={styles.page}>
      <Title title={'Daftar Mobil'} />
    </View>
  );

  return (
    <SafeAreaView style={{backgroundColor: backgroundColor, flex: 1}}>
      <CarList header={header} />
    </SafeAreaView>
  );
};

export default Mobil;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    alignItems: 'center',
  },
});
