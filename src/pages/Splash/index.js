import {
  StyleSheet,
  SafeAreaView,
  StatusBar,
  ImageBackground,
  Text,
  View,
  Dimensions,
} from 'react-native';
import React, {useEffect} from 'react';
import {Mercedes} from '../../assets';
import {primaryColor, secondaryColor} from '../../utils/constant';

const Splash = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('MainApp');
    }, 1200);
  });

  return (
    <SafeAreaView style={[styles.container]}>
      <StatusBar backgroundColor={secondaryColor} />
      <ImageBackground style={styles.background}>
        <View style={styles.titleBackground}>
          <Text style={styles.title}>BCR</Text>
          <Text style={styles.title}>Binar Car Rental</Text>
        </View>
        <View style={styles.carBackground}>
          <Mercedes
            height={window.height * 0.27}
            width={window.width * 1}
            style={styles.car}
          />
        </View>
      </ImageBackground>
    </SafeAreaView>
  );
};

export default Splash;

const window = Dimensions.get('screen');

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  background: {
    flex: 1,
    backgroundColor: secondaryColor,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  title: {
    fontFamily: 'helvetica',
    fontSize: 36,
    fontWeight: 'bold',
    color: 'white',
  },
  titleBackground: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 4,
  },
  car: {
    marginTop: -window.width * 0.22,
  },
  carBackground: {
    borderTopLeftRadius: 60,
    width: '100%',
    flex: 1,
    backgroundColor: primaryColor,
    alignItems: 'center',
  },
});
