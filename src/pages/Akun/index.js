import {StyleSheet, Text, View, Image, Dimensions} from 'react-native';
import {Park} from '../../assets';
import Button from '../../components/Button';
import Title from '../../components/Title';
import React from 'react';
import {backgroundColor} from '../../utils/constant';

const Akun = () => {
  return (
    <View style={styles.page}>
      <Title title={'Akun'} />
      <View style={styles.pageContainer}>
        <Park height={window.height * 0.3} width={window.width} />
        <Text style={styles.detailText}>
          Upss kamu belum memiliki akun. Mulai buat akun agar transaksi di BCR
          lebih mudah
        </Text>
        <Button title={'Register'} />
      </View>
    </View>
  );
};

export default Akun;

const window = Dimensions.get('window');

const styles = StyleSheet.create({
  page: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: backgroundColor,
  },
  pageContainer: {
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 24,
  },
  image: {
    width: window.width * 0.9,
    height: window.width * 0.5,
    margin: 25,
  },
  detailText: {
    fontFamily: 'Helvetica',
    fontWeight: '300',
    textAlign: 'center',
    alignSelf: 'stretch',
    fontSize: 20,
    margin: 24,
    color: 'black',
  },
});
