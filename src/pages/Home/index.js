import {StyleSheet, SafeAreaView, View, StatusBar} from 'react-native';
import React from 'react';
import Title from '../../components/Title';
import {Banner, CarList, Header, Menu} from '../../components';
import {backgroundColor, primaryColor} from '../../utils/constant';
import {useIsFocused} from '@react-navigation/native';

const Home = () => {
  const header = (
    <View style={styles.page}>
      <StatusBar
        backgroundColor={useIsFocused() ? primaryColor : backgroundColor}
        barStyle={'dark-content'}
      />
      <Header name={'Yobel'} location={'Indonesia'} />
      <Banner />
      <Menu />
      <Title title={'Daftar Mobil Pilihan'} />
    </View>
  );

  return (
    <SafeAreaView style={{backgroundColor: backgroundColor, flex: 1}}>
      <CarList header={header} />
    </SafeAreaView>
  );
};

export default Home;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    alignItems: 'center',
  },
});
